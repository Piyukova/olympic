<?php
/*
Plugin Name: MIF Olympic Plugin
Plugin URI: https://bitbucket.org/Piyukova/olympic
Description: Плагин для создания сайтов открытых олимпиад.
Author: Наталья Пиюкова
Version: 0.1
Author URI: ....
*/



// Функция запускает после загрузки WordPress и перед выводом чего-либо на экран
// Здесь можно определять свои типы записей, назначить шорткоды и др. 
// Подробнее (все хуки и фильтры): http://wp-kama.ru/hooks

add_action( 'init', 'mif_olympic_init' );

function mif_olympic_init() 
{

    // В примере - создается новый тип записей "Олимпиада" (olympic)
    // Подробнее - http://wp-kama.ru/function/register_post_type

    register_post_type( 'olympic', array(
        'labels' => array(
        'name'            => __( 'Olympics' ),
        'singular_name'   => __( 'Olympics' ),
        'add_new'         => __( 'Add new olympics' ),
        'add_new_item'    => __( 'Add new olympics item' ),
        'edit'            => __( 'Edit olympics' ),
        'edit_item'       => __( 'Edit olympics item' ),
        'new_item'        => __( 'New olympics item' ),
        'all_items'       => __( 'All olympics item' ),
        'view'            => __( 'View olympics' ),
        'view_item'       => __( 'View olympics item' ),
        'search_items'    => __( 'Search olympics item' ),
        'not_found'       => __( 'Olympics not found' ),
    ),
    'public' => true, 
    'menu_position' => 20,
    'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields'),
    'taxonomies' => array( '' ),
    'has_archive' => true,
    'capability_type' => 'post',
    'menu_icon'   => 'dashicons-smiley',
    'rewrite' => array('slug' => 'olympics'),
    ));    
    // !!! Изменения вступают в силу после нажания кнопки "Сохранить" на странице /wp-admin/options-permalink.php  

}


// Подключаем свой файл CSS
// Подробнее: http://wp-kama.ru/function/wp_register_style

add_action( 'wp_enqueue_scripts', 'add_olympics_plugin_styles' );

function add_olympics_plugin_styles() {
	wp_register_style( 'olympics-plugin-styles', plugins_url( 'styles.css', __FILE__ ) );
	wp_enqueue_style( 'olympics-plugin-styles' );
}



// Функция получает текст записи, который можно изменить перед выводом на экран

add_filter ('the_content', 'add_olympic_content');

function add_olympic_content( $content ) 
{
    global $post;

    // Здесь проверяем - указан ли режим работы. Устанавливаем режим.
    if ( isset($_GET['mode']) ) { $mode = $_GET['mode']; } else { $mode = ''; }; 

    if ( $post->post_type == 'olympic' ) {

        if ( isset($_POST['submit']) && wp_verify_nonce( $_POST['save_olympics_nonce'], 'save_olympics' )) {
            __save_data();
            $content .= note( 'Спасибо, ваша заявка учтена!' );
        } else { 


            $content .= '<a href="' . get_permalink() . '">Обычный текст</a> - <a href="?mode=new_member">Регистрация</a> - <a href="?mode=another">Полезное</a>';

            // Проверяем режим и выводим то, что надо
            if ( $mode == 'new_member' ) {

                $content .= '<p><h3>Новая заявка:</h3>' . __create_form(); 

            } elseif ( $mode == 'another' ) {

                $content .= '<p>Что-то другое, очень полезное'; 
                
            } else {

                $content .= '<p>Обычный текст'; 
                
            }

        }

        
        $olympics_data_arr = get_post_meta( $post->ID, 'olympics_data' );
        if ( $olympics_data_arr ) {
            $content .= '<p><h3>Участники олимпиады:</h3>' . __get_participant( $olympics_data_arr );
        } 
        
        
        


    }

    return $content;

}

function note( $txt, $class = 'note' )
{
    $out = '<div class="' . $class . '">' . $txt . '</div>';

    return $out;
}


function __get_participant( $arr )
{
    $out ='';

    foreach ( (array)$arr as $item ) {

        $out .= '<p>' . $item['firstname'] . ' ' . $item['secondname'];

    }

    return $out;
}


function __save_data()
{
    global $post;
    $data = $_POST;
    add_post_meta( $post->ID, 'olympics_data', $data );
}


function __create_form()
{
    $out = '';

    $out .= '<p><form method="POST">
    <p>Ваше имя: <input type="text" name="firstname">
    <p>Ваша фамилия: <input type="text" name="secondname">
    <p><input type="submit" name="submit" value="Отправить заявку">' .
    wp_nonce_field( 'save_olympics','save_olympics_nonce' ) . '
    </form>';

    return $out;
}















function add_custom_content_old( $content ) 
{
    global $post;

    if ( $post->post_type == 'olympics' ) {
        // Делаем изменения только для зарегистрированного нами типа записей


        // Проверяем, не пытается ли пользователь сохранить новые данные. Если да, то сохраняем их.
        if ( isset($_POST['name']) && is_user_logged_in() && wp_verify_nonce( $_POST['save_questionnaire_nonce'], 'save_questionnaire' ) ) 
            add_post_meta( $post->ID, 'questionnaire_data', array(  'name' => $_POST['name'], 
                                                                    'city' => $_POST['city'], 
                                                                    'question' => $_POST['question'] ) );

        // Читаем все сохранённые данные анкет
        $questionnaire_data_arr = get_post_meta( $post->ID, 'questionnaire_data' );

        // Выводим анкетную форму, но только для зарегистрированных пользователей
        if ( is_user_logged_in() ) {

            $content .= '<form method="POST">
            <p>Ваше имя?<br /> <input type="text" name="name">
            <p>В каком городе вы живете?<br /> <input type="text" name="city">
            <p>Какой вопрос вы хотите задать?<br /> <textarea name="question"></textarea>
            <p><input type="submit" value="Сохранить">';
            $content .= wp_nonce_field( 'save_questionnaire','save_questionnaire_nonce' );
            $content .= '</form>';

        } else {

            $content .= '<p class="warning">Пройдите регистрацию, если хотите задать вопрос';

        }

        // Выводим данные, которые были сохранены ранее
        foreach ( (array)$questionnaire_data_arr as $item ) 
            $content .= '<p class="questionnaire"><em>' . $item['name'] . ' (' . $item['city'] . ')</em><br />' . $item['question']; 
        
    }
    

    return $content;
}



?>